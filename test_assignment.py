from util import CustomTest
from collections.abc import Collection
import pytest
from assignment import evaluate_term, evaluate_atom

# Collect the test cases
test_cases = [
    (test.input, test.output, test.func, test.name) for test in CustomTest._tests
]
print(test_cases)
# Debugging: Print collected test cases
print(f"Collected test cases: {test_cases}")


@pytest.mark.parametrize(
    "input_values, expected_output, func, name",
    test_cases,
    ids=[name or func.__name__ for (_, _, func, name) in test_cases],
)
def test_custom(input_values, expected_output, func, name):
    """
    Test function for custom tests. This will be dynamically parameterized.
    """
    # Debugging: Print details of the test case being executed
    print(f"Running test: {name or func.__name__} with input {input_values}")

    try:
        result = func(*input_values)
        if isinstance(expected_output, Collection) and not isinstance(
            expected_output, str
        ):
            assert (
                result in expected_output
            ), f"{func.__name__}{input_values} returned {result} which is not in {expected_output}"
        elif callable(expected_output):
            assert expected_output(
                result
            ), f"{func.__name__}{input_values} returned {result} which did not satisfy {expected_output}"
        else:
            assert (
                result == expected_output
            ), f"{func.__name__}{input_values} returned {result} instead of {expected_output}"
    except Exception as e:
        pytest.fail(f"{func.__name__}{input_values} raised an exception: {e}")
